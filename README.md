# Personal WebSite
Gabriel Morais - Personal Web Portifolio

## Getting started
To access the content of this Portifolio just need to download the Sorce Code and click index.html

## Name
Content for class COMP_SCI-490WD -- University Of Missouri Kansas City

## License
Open source project.

## Template Author
  =>  Template Name: FreeFolio - Freelancer Portfolio Template

  =>  Template Link: https://htmlcodex.com/freelancer-portfolio-template

  =>  Template License: https://htmlcodex.com/license (or read the LICENSE.txt file)

  =>  Template Author: HTML Codex

  =>  Author Website: https://htmlcodex.com

  =>  About HTML Codex: HTML Codex is one of the top creators and publishers of Free HTML templates, HTML landing pages, HTML email templates and HTML snippets in the world. Read more at ( https://htmlcodex.com/about-us )
